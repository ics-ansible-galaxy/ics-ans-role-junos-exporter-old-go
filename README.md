# ics-ans-role-junos-exporter

Installs the junos exporter from https://github.com/czerwonk/junos_exporter.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-exporter
```

## License

BSD 2-clause
